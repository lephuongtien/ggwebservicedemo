//
//  AppDelegate.h
//  GGWebServiceDemo
//
//  Created by Tien Le Phuong on 6/3/14.
//  Copyright (c) 2014 Le Phuong Tien. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
