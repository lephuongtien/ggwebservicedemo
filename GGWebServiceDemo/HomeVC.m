//
//  HomeVC.m
//  GGWebServiceDemo
//
//  Created by Tien Le Phuong on 6/4/14.
//  Copyright (c) 2014 Le Phuong Tien. All rights reserved.
//

#import "HomeVC.h"
#import "GGWebService.h"
#import "GGDownloader.h"

#import "FXDownloader.h"

@interface HomeVC ()<GGWebServiceDelegate>
{
    BOOL _changeDownloadImage;
}

@property(nonatomic, weak) IBOutlet UIActivityIndicatorView  *indcator;
@property(nonatomic, weak) IBOutlet UIImageView *imageView;
@property(nonatomic, weak) IBOutlet UIImageView *imageView2;
@property(nonatomic, weak) IBOutlet UIImageView *imageView3;

@end

@implementation HomeVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)callWithDelegate:(id)sender
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://dev.youlook.net/api/nodes/landingpage?page=1&limit=20"]];
    
    GGWebService *ws = [[GGWebService alloc] init];
    ws.delegate = self;
    [ws callWebServiceWithRequest:request];

}

- (IBAction)callWithDelegateIdentifier:(id)sender
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://dev.youlook.net/api/nodes/landingpage?page=1&limit=20"]];
    
    GGWebService *ws = [[GGWebService alloc] init];
    ws.delegate = self;
    [ws callWebServiceWithRequest:request identifier:@"request ne"];
    
}

- (IBAction)callWithBlock:(id)sender
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://itunes.apple.com/us/rss/topaudiobooks/limit=100/json"]];
    
    GGWebService *ws = [[GGWebService alloc] init];
    [ws callWebServiceWithRequest:request
                    completeBlock:^(NSData *data, NSError *error) {
                        
                        if (error) {
                            NSLog(@"Error: %@",error);
                        } else {
                            NSLog(@"Data: %@",[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil]);
                        }
                        
                        NSLog(@"CALL WEBSERVICE IS DONE");
                        
                    }];

}

- (IBAction)callUsingClassReques:(id)sender
{
    [GGWebService callWebServiceWithRequest:[GGWSRequest requestSimpleGETstringURL:@"http://dev.youlook.net/api/nodes/landingpage?page=1&limit=20"]
                              completeBlock:^(NSData *data, NSError *error) {
                                  
                                  
                                  if (error) {
                                      NSLog(@"Error: %@",error);
                                  } else {
                                      NSLog(@"Data: %@",[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil]);
                                  }
                                  
                                  NSLog(@"CALL WEBSERVICE IS DONE");
                              }];


}

- (IBAction)callUsingClassParse:(id)sender
{
    [GGWebService callWebServiceWithRequest:[GGWSRequest requestSimpleGETstringURL:@"http://dev.youlook.net/api/nodes/landingpage?page=1&limit=20"]
                              completeBlock:^(NSData *data, NSError *error) {
                                  
                                  
                                  if (error) {
                                      NSLog(@"Error: %@",error);
                                  } else {
                                      NSLog(@"Data: %@",[GGWSParse parseDataJSONWithData:data]);
                                  }
                                  
                                  NSLog(@"CALL WEBSERVICE IS DONE");
                              }];

}

- (IBAction)callUsingClassParseBlock:(id)sender
{
    [GGWebService callWebServiceWithRequest:[GGWSRequest requestSimpleGETstringURL:@"http://dev.youlook.net/api/nodes/landingpage?page=1&limit=20"]
                              completeBlock:^(NSData *data, NSError *error) {
                                  
                                  
                                  if (error) {
                                      NSLog(@"Error: %@",error);
                                  } else {
                                      
                                      [GGWSParse parseDataJSONData:data WithComplete:^(NSDictionary *dic, GGWSError *error) {
                                          
                                          if (error) {
                                              NSLog(@"error: %@", error.error_msg);
                                          } else {
                                              NSLog(@"data: %@",dic);
                                              NSLog(@"cdn: %@", [dic stringForKey:@"cdn"]);
                                              NSLog(@"code: %d", [dic intForKey:@"code"]);
                                          }
                                          
                                      }];
                                      
                                  }
                                  
                                  NSLog(@"CALL WEBSERVICE IS DONE");
                              }];
}

#pragma mark - Delegate

- (void)webService:(GGWebService*)ws didSuccessWith:(NSData*)data
{
    NSDictionary *dic =  [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    NSLog(@"%@", dic);
    
    NSLog(@"DONE WS");
}

- (void)webService:(GGWebService*)ws didFailWith:(NSError*)error
{
    NSLog(@"%@", error);
    
    NSLog(@"DONE WS");
}

- (void)webService:(GGWebService*)ws didSuccessWith:(NSData*)data identifier:(id)identifier
{
    NSLog(@"identifier: %@",identifier);
    
    NSLog(@"DONE WS");
}

- (void)webService:(GGWebService*)ws didFailWith:(NSError*)error identifier:(id)identifier
{
    NSLog(@"error identifier: %@",identifier);
    
    NSLog(@"DONE WS");
}

#pragma mark - Downloader
- (IBAction)downloadImage:(id)sender
{
    
    NSString *urlImage = (_changeDownloadImage) ? @"http://streamcity.us/media/8bad2e3e43617df6529ffcd0fa388a63-6.jpg" : @"http://1.bp.blogspot.com/-1_bRhraOo90/Ujf9vXwsqgI/AAAAAAAABcw/H0IMOT8Epo8/s1600/hot-girl-14t-3.jpg";
    
    // down  image 1
    [GGDownloader downLoadImageStringURL:urlImage
                       completeWithBlock:^(UIImage *image, GGWSError *error) {
                           _imageView.image = image;

    }];
    
    // down  image 2
    [_imageView2 setImageWithURL:[NSURL URLWithString:urlImage] placeholderImage:[UIImage imageNamed:NO_IMAGE_DEFAULT]];
    
    //down image 3
    [FXDownloader downLoadImageStringURL:urlImage completeWithBlock:^(UIImage *image, FXError *error) {
        _imageView3.image = image;
    }];
    
    _changeDownloadImage = !_changeDownloadImage;
}



@end
