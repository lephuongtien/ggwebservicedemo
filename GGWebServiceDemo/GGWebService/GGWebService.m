//
//  GGWebService.m
//  YouLook
//
//  Created by Tien Le Phuong on 6/3/14.
//  Copyright (c) 2014 TienLP. All rights reserved.
//

#import "GGWebService.h"

@interface GGWebService ()
{
    
}

@end

@implementation GGWebService

+ (GGWebService*)sharedGGWebService
{
    static GGWebService *sharedGGWebService = nil;
    
    @synchronized(self) {
        if (sharedGGWebService == nil)
            sharedGGWebService = [[self alloc] init];
    }
    
    return sharedGGWebService;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (id)initWithStringURL:(NSString*)stringURL
{
    self = [super init];
    if (self) {
        self.strURL = stringURL;
    }
    return self;
}

- (id)initWithURL:(NSURL*)url
{
    self = [super init];
    if (self) {
        self.url = url;
    }
    return self;
}

- (id)initWithURLRequest:(NSURLRequest*)urlRequest
{
    self = [super init];
    if (self) {
        self.urlRequest = urlRequest;
    }
    return self;
}

- (void)dealloc
{
    
}


#pragma mark - Privater Methods

- (void)callWSBG
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    if (![self isNetworkReachability]) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
           
            AFHTTPRequestOperation *operation   = [[AFHTTPRequestOperation alloc] initWithRequest:self.urlRequest];
            operation.responseSerializer        = [AFJSONResponseSerializer serializer];
            
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                
                //call Success
                NSData *responseData = operation.responseData;
                
                
                if (_isBlock) {
                    
                    _complete(responseData,nil);
                    self.isBlock = NO;
                    
                } else {
                    if (_delegate) {
                        
                        if ([_delegate respondsToSelector:@selector(webService:didSuccessWith:)]) {
                            
                            [_delegate webService:self didSuccessWith:responseData];
                            
                        }
                        
                        if ([_delegate respondsToSelector:@selector(webService:didSuccessWith:identifier:)])  {
                            
                            [_delegate webService:self didSuccessWith:responseData identifier:self.identifier];
                            
                        }
                        
                    }
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                
                //call fail
                if (_isBlock) {
                    
                    _complete(nil,error);
                    self.isBlock = NO;
                    
                } else {
                    if (_delegate) {
                        
                        if ([_delegate respondsToSelector:@selector(webService:didFailWith:)]) {
                            
                            [_delegate webService:self didFailWith:error];
                            
                        }
                        
                        if ([_delegate respondsToSelector:@selector(webService:didFailWith:identifier:)]) {
                            
                            [_delegate webService:self didFailWith:error identifier:self.identifier];
                            
                        }
                        
                        
                    }
                }
                
                
            }];
            
            [operation start];
            
        });
        
        
    } else {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSDictionary *userInfo = @{@"message" : @"Network is not Reachability."};
        NSError *error = [NSError errorWithDomain:@"" code:1 userInfo:userInfo];
        
        //call fail
        if (_isBlock) {
            
            _complete(nil,error);
            self.isBlock = NO;
            
        } else {
            if (_delegate) {
                
                if ([_delegate respondsToSelector:@selector(webService:didFailWith:)]) {
                    
                    [_delegate webService:self didFailWith:error];
                    
                }
                
                if ([_delegate respondsToSelector:@selector(webService:didFailWith:identifier:)]) {
                    
                    [_delegate webService:self didFailWith:error identifier:self.identifier];
                    
                }
                
                
            }
        }
        
    }
}

#pragma mark - Public Methods

- (BOOL) isNetworkReachability
{
    return [AFNetworkReachabilityManager sharedManager].reachable;
}

- (void) callWebServiceWithStringUrl:(NSString*)strURL
{
    self.urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:strURL]];
    
    [self callWSBG];
}

- (void) callWebServiceWithStringUrl:(NSString*)strURL identifier:(id)identifier
{
    self.urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:strURL]];
    self.identifier = identifier;
    
    [self callWSBG];
}

- (void) callWebServiceWithRequest:(NSURLRequest*)urlRequest
{
    self.urlRequest = urlRequest;
    
    [self callWSBG];
}

- (void) callWebServiceWithRequest:(NSURLRequest*)urlRequest identifier:(id)identifier
{
    self.urlRequest = urlRequest;
    self.identifier = identifier;
    
    [self callWSBG];
}

- (void) callWebServiceWithStringUrl:(NSString*)strURL completeBlock:(GGWebServiceComplete)complete
{
    self.urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:strURL]];
    self.isBlock    = YES;
    _complete       = complete;
    
    [self callWSBG];
}

- (void) callWebServiceWithRequest:(NSURLRequest*)urlRequest completeBlock:(GGWebServiceComplete)complete
{
    self.urlRequest = urlRequest;
    self.isBlock    = YES;
    _complete       = complete;
    
    [self callWSBG];
}

#pragma mark - Class Method
+ (void) callWebServiceWithStringUrl:(NSString*)strURL completeBlock:(GGWebServiceComplete)complete
{
    [[[GGWebService alloc] init] callWebServiceWithStringUrl:strURL completeBlock:^(NSData *data, NSError *error) {
        complete(data, error);
    }];
}

+ (void) callWebServiceWithRequest:(NSURLRequest*)urlRequest completeBlock:(GGWebServiceComplete)complete
{
    [[[GGWebService alloc] init] callWebServiceWithRequest:urlRequest completeBlock:^(NSData *data, NSError *error) {
        complete(data, error);
    }];
}












@end
