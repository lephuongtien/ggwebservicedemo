//
//  GGDownloader.m
//  GGWebServiceDemo
//
//  Created by Tien Le Phuong on 6/4/14.
//  Copyright (c) 2014 Le Phuong Tien. All rights reserved.
//

#import "GGDownloader.h"

@interface GGDownloader()
{
    
}

@end

@implementation GGDownloader

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (id)initWithStringURL:(NSString*)stringURL
{
    self = [super init];
    if (self) {
        self.strURL             = stringURL;
    }
    return self;
}

- (id) initWithStringURL:(NSString*)stringURL target:(id)target
{
    self = [super init];
    if (self) {
        self.strURL             = stringURL;
        self.target             = target;
        _isRequestWithTagert    = YES;
    }
    return self;
}

- (id) initWithStringURL:(NSString*)stringURL target:(id)target identifier:(id)identifier
{
    self = [super init];
    if (self) {
        self.strURL             = stringURL;
        self.target             = target;
        self.identifier         = identifier;
        _isRequestWithTagert    = YES;
    }
    return self;
}

- (id) initWithStringURL:(NSString*)stringURL target:(id)target identifier:(id)identifier indexPath:(NSIndexPath*)indexPath
{
    self = [super init];
    if (self) {
        self.strURL             = stringURL;
        self.target             = target;
        self.identifier         = identifier;
        self.indexPath          = indexPath;
        _isRequestWithTagert    = YES;
    }
    return self;
}

- (void)dealloc
{
    
}

#pragma mark - Setter
- (void) setStrURL:(NSString *)strURL
{
    _strURL = strURL;
    _url = [NSURL URLWithString:strURL];
}

#pragma mark - Getter

- (NSMutableURLRequest*) urlRequest
{
    if (!_urlRequest) {
        _urlRequest   = [NSMutableURLRequest requestWithURL:self.url];
        [_urlRequest addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    }
    
    return _urlRequest;
}

#pragma mark - public method

- (void)setCompleteWithBlock:(GGDownloaderComplete)complete
{
    _complete = [complete copy];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        AFHTTPRequestOperation *operation   = [[AFHTTPRequestOperation alloc] initWithRequest:self.urlRequest];
        operation.responseSerializer        = [AFImageResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            
            
            //call Success
            NSData *responseData = operation.responseData;
            
            if (_isRequestWithTagert && !self.target) {
                return;
            }
            
            _complete(responseData, nil);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            
            if (_isRequestWithTagert && !self.target) {
                return;
            }
            
            GGWSError *ggError  = [GGWSError error];
            ggError.error_id    = 1;
            ggError.error_msg   = error.description;
            
            _complete(nil, ggError);
            
            
        }];
        
        [operation start];
        
    });

    
}

- (void) setImageCompleteWithBlock:(GGDownloaderImageComplete)imageComplete
{
    _imageComplete = [imageComplete copy];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        AFHTTPRequestOperation *operation   = [[AFHTTPRequestOperation alloc] initWithRequest:self.urlRequest];
        operation.responseSerializer        = [AFImageResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            //call Success
            NSData *data = operation.responseData;
            
            if (_isRequestWithTagert && !self.target) {
                return;
            }
            
            if ([data length]) {
                _imageComplete([UIImage imageWithData:data], nil);
            } else {
                GGWSError *ggerror = [GGWSError error];
                ggerror.error_id = 1;
                ggerror.error_msg = @"Image is 0 byte.";
                
                _imageComplete([UIImage imageNamed:NO_IMAGE_DEFAULT], ggerror);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            
            if (_isRequestWithTagert && !self.target) {
                return;
            }
            
            GGWSError *ggError  = [GGWSError error];
            ggError.error_id    = 1;
            ggError.error_msg   = error.description;
            
            _imageComplete([UIImage imageNamed:NO_IMAGE_DEFAULT], ggError);
            
            
        }];
        
        [operation start];
        
    });
    
}

- (void) setCellImageCompleteWithBlock:(GGDownloaderCellImageComplete)cellImageComplete
{
    _cellImageComplete = [cellImageComplete copy];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        AFHTTPRequestOperation *operation   = [[AFHTTPRequestOperation alloc] initWithRequest:self.urlRequest];
        operation.responseSerializer        = [AFImageResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            
            
            //call Success
            NSData *data = operation.responseData;
            
            if (_isRequestWithTagert && !self.target) {
                return;
            }
            
            if ([data length]) {
                
                _cellImageComplete([UIImage imageWithData:data], nil, _indexPath, _identifier);
            } else {
                GGWSError *error = [GGWSError error];
                error.error_id = 1;
                error.error_msg = @"Image is 0 byte.";
                
                _cellImageComplete([UIImage imageNamed:NO_IMAGE_DEFAULT], error, _indexPath, _identifier);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            
            if (_isRequestWithTagert && !self.target) {
                return;
            }
            
            GGWSError *ggError  = [GGWSError error];
            ggError.error_id    = 1;
            ggError.error_msg   = error.description;
            
            _cellImageComplete([UIImage imageNamed:NO_IMAGE_DEFAULT], ggError, _indexPath, _identifier);
            
            
        }];
        
        [operation start];
        
    });
    
}

#pragma mark - Class method
+ (void) downloadWithStringURL:(NSString*)stringURL
             completeWithBlock:(GGDownloaderComplete)complete
{
    return [[[self alloc] initWithStringURL:stringURL] setCompleteWithBlock:complete];
}

+ (void) downloadWithStringURL:(NSString*)stringURL
                        target:(id)target
             completeWithBlock:(GGDownloaderComplete)complete
{
    return [[[self alloc] initWithStringURL:stringURL target:target] setCompleteWithBlock:complete];
}

+ (void) downloadWithStringURL:(NSString*)stringURL
                        target:(id)target
                    identifier:(id)identifier
             completeWithBlock:(GGDownloaderComplete)complete
{
    return [[[self alloc] initWithStringURL:stringURL target:target identifier:identifier] setCompleteWithBlock:complete];
}

+ (void) downloadWithStringURL:(NSString*)stringURL
                        target:(id)target
                    identifier:(id)identifier
                     indexPath:(NSIndexPath*)indexPath
             completeWithBlock:(GGDownloaderComplete)complete
{
    return [[[self alloc] initWithStringURL:stringURL target:target identifier:identifier indexPath:indexPath] setCompleteWithBlock:complete];
}

#pragma mark - Download Image
+ (void) downLoadImageStringURL:(NSString*)stringURL
              completeWithBlock:(GGDownloaderImageComplete)complete
{
    return [[[self alloc] initWithStringURL:stringURL] setImageCompleteWithBlock:complete];
}

+ (void) downLoadImageStringURL:(NSString*)stringURL
                         target:(id)target
              completeWithBlock:(GGDownloaderImageComplete)complete
{
    return [[[self alloc] initWithStringURL:stringURL target:target] setImageCompleteWithBlock:complete];
}

+ (void) downLoadImageStringURL:(NSString*)stringURL
                         target:(id)target
                     identifier:(id)identifier
              completeWithBlock:(GGDownloaderImageComplete)complete
{
    return [[[self alloc] initWithStringURL:stringURL target:target identifier:identifier] setImageCompleteWithBlock:complete];
}

+ (void) downLoadImageStringURL:(NSString*)stringURL
                         target:(id)target
                     identifier:(id)identifier
                      indexPath:(NSIndexPath*)indexPath
              completeWithBlock:(GGDownloaderImageComplete)complete
{
    return [[[self alloc] initWithStringURL:stringURL target:target identifier:identifier indexPath:indexPath] setImageCompleteWithBlock:complete];
}

#pragma mark - cell downloader
+ (void) cellDownloadImageWithStringURL:(NSString*)stringURL
                                 target:(id)target
                             identifier:(id)identifier
                              indexPath:(NSIndexPath*)indexPath
                      completeWithBlock:(GGDownloaderCellImageComplete)complete
{
    return [[[self alloc] initWithStringURL:stringURL target:target identifier:identifier indexPath:indexPath] setCellImageCompleteWithBlock:complete];
}

@end
