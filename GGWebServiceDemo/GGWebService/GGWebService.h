//
//  GGWebService.h
//  YouLook
//
//  Created by Tien Le Phuong on 6/3/14.
//  Copyright (c) 2014 TienLP. All rights reserved.
//
//  Note: Using Framework AFNet

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

#import "GGWSRequest.h"
#import "GGWSParse.h"

typedef void (^GGWebServiceComplete)(NSData *data, NSError *error);

@protocol GGWebServiceDelegate;

@interface GGWebService : NSObject
{
    GGWebServiceComplete _complete;
}

//property
@property (nonatomic) BOOL isBlock;

@property (nonatomic, strong) NSURLRequest        *urlRequest;
@property (nonatomic, strong) NSString            *strURL;
@property (nonatomic, strong) NSURL               *url;

@property (nonatomic, strong) id                  identifier;

//delegate
@property(nonatomic,weak) id<GGWebServiceDelegate>delegate;

//public method
- (BOOL) isNetworkReachability;

- (void) callWebServiceWithStringUrl:(NSString*)strURL;
- (void) callWebServiceWithStringUrl:(NSString*)strURL identifier:(id)identifier;

- (void) callWebServiceWithRequest:(NSURLRequest*)urlRequest;
- (void) callWebServiceWithRequest:(NSURLRequest*)urlRequest identifier:(id)identifier;

- (void) callWebServiceWithStringUrl:(NSString*)strURL completeBlock:(GGWebServiceComplete)complete;
- (void) callWebServiceWithRequest:(NSURLRequest*)urlRequest completeBlock:(GGWebServiceComplete)complete;

//init
- (id)initWithStringURL:(NSString*)stringURL;
- (id)initWithURL:(NSURL*)url;
- (id)initWithURLRequest:(NSURLRequest*)urlRequest;

//class method
+ (void) callWebServiceWithStringUrl:(NSString*)strURL completeBlock:(GGWebServiceComplete)complete;
+ (void) callWebServiceWithRequest:(NSURLRequest*)urlRequest completeBlock:(GGWebServiceComplete)complete;

//instance
+ (GGWebService*)sharedGGWebService;

@end

@protocol GGWebServiceDelegate <NSObject>

@required

- (void)webService:(GGWebService*)ws didSuccessWith:(NSData*)data;
- (void)webService:(GGWebService*)ws didFailWith:(NSError*)error;

@optional

- (void)webService:(GGWebService*)ws didSuccessWith:(NSData*)data identifier:(id)identifier;
- (void)webService:(GGWebService*)ws didFailWith:(NSError*)error identifier:(id)identifier;

@end
