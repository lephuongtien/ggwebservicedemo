//
//  GGWSParse.m
//  YouLook
//
//  Created by Tien Le Phuong on 6/4/14.
//  Copyright (c) 2014 TienLP. All rights reserved.
//

#import "GGWSParse.h"

@implementation GGWSParse

#pragma mark - Basic Parse Data

+ (NSMutableDictionary*) parseDataJSONWithData:(NSData*)data
{
    NSString *tempStr = [[NSString alloc] initWithBytes: [data bytes]
                                                 length:[data length]
                                               encoding:NSUTF8StringEncoding];
    //NSLog(@"%@", tempStr);
    
    return [tempStr JSONValue];
}

+ (void) parseDataJSONData:(NSData*)data WithComplete:(GGWSParseBasic)complete
{
    NSString *tempStr   = [[NSString alloc] initWithBytes: [data bytes]
                                                 length:[data length]
                                               encoding:NSUTF8StringEncoding];
    //NSLog(@"%@", tempStr);
    
    NSDictionary *dic   = [tempStr JSONValue];
    GGWSError *error    = [GGWSError error];
    
    if (dic) {
        complete(dic, nil);
    } else {
        
        error.error_id  = 1;
        error.error_msg = @"Data error dic.";
        
        complete(nil, error);
    }
    
}

+ (NSDictionary*) parseDataXMLWithData:(NSData*)data
{
    return [NSDictionary dictionaryWithXMLData:data];
}

+ (void) parseDataXMLData:(NSData*)data WithComplete:(GGWSParseBasic)complete
{
    NSDictionary *dic = [NSDictionary dictionaryWithXMLData:data];
    
    GGWSError *error    = [GGWSError error];
    
    if (dic) {
        complete(dic, nil);
    } else {
        
        error.error_id  = 1;
        error.error_msg = @"Data error dic.";
        
        complete(nil, error);
    }
}

@end
