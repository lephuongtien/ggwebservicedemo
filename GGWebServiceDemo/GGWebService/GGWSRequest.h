//
//  GGWSRequest.h
//  YouLook
//
//  Created by Tien Le Phuong on 6/3/14.
//  Copyright (c) 2014 TienLP. All rights reserved.
//

#import <Foundation/Foundation.h>


//define METHOD
#define GGWSRequestMethodPOST               @"POST"
#define GGWSRequestMethodGET                @"GET"
#define GGWSRequestMethodPUT                @"PUT"
#define GGWSRequestMethodDELETE             @"DELETE"

//define TIME_OUT
#define GGWSRequestTimeOutDefault           60

//ACCESS_TOKEN
#define ACCESS_TOKEN @"Access-Token"

#define AUTHEN_USERNAME @"tesssss"
#define AUTHEN_PASSWORD @"tesssss"

@interface GGWSRequest : NSObject

//defaulf request
+ (NSURLRequest*) requestSimpleGETstringURL:(NSString*)strURL;

+ (NSMutableURLRequest *)requestWithMethod:(NSString *)method
                                 URLString:(NSString *)URLString
                                parameters:(id)parameters;

//custom request
+ (NSURLRequest*) requestGETstringURL:(NSString*)strURL isAccessToken:(BOOL)isAccessToken isAuthorization:(BOOL)isAuthorization;

@end
