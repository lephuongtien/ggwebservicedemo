//
//  GGWSError.m
//  YouLook
//
//  Created by Tien Le Phuong on 6/4/14.
//  Copyright (c) 2014 TienLP. All rights reserved.
//

#import "GGWSError.h"

//DEFINE ID NOTIFICATION
#define _gg_idError1 404

//DEFINE MESSAGE NOTIFICATION
#define _gg_messageError1 @"This is demo error."

@interface GGWSError()
{
    
}

@property(nonatomic, strong) NSMutableDictionary *idNotifications;
@property(nonatomic, strong) NSMutableDictionary *messageNotifications;

@end

@implementation GGWSError

- (id) init
{
    self = [super init];
    if (self) {
        _error_id       = 0;
        _error_msg      = @"";
    }
    return self;
}

- (void)dealloc
{
    self.error_msg              = nil;
    self.idNotifications        = nil;
    self.messageNotifications   = nil;
}

+ (GGWSError*) error
{
    GGWSError *error = [[GGWSError alloc] init];
    return error;
}

#pragma mark - Notification

- (NSMutableDictionary*) idNotifications
{
    
    if (!_idNotifications) {
        _idNotifications = [[NSMutableDictionary alloc] init];
    }
    
    return _idNotifications;
}

- (NSMutableDictionary*) messageNotifications
{
    if (!_messageNotifications) {
        _messageNotifications = [[NSMutableDictionary alloc] init];
    }
    
    return _messageNotifications;
}

- (void) postNotification
{
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjects:@[[NSNumber numberWithInt:_error_id], _error_msg] forKeys:@[@"error_id", @"error_msg"]];
    [[NSNotificationCenter defaultCenter] postNotificationName:FXErrorNotification object:nil userInfo:userInfo];
    
}

#pragma mark - Setter

- (void)setError_id:(int)error_id
{
    _error_id = error_id;
    
    //check
    switch (error_id) {
        case _gg_idError1:
        {
            [self postNotification];
            break;
        }
        default:
            break;
    }
}

- (void)setError_msg:(NSString *)error_msg
{
    _error_msg = error_msg;
    
    //check
    if ([error_msg isEqualToString:_gg_messageError1]) {
        [self postNotification];
    }
}

@end
