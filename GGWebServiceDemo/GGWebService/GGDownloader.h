//
//  GGDownloader.h
//  GGWebServiceDemo
//
//  Created by Tien Le Phuong on 6/4/14.
//  Copyright (c) 2014 Le Phuong Tien. All rights reserved.
//
//  Note: Using Framework AFNet

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"

#import "GGWSError.h"

#define NO_IMAGE_DEFAULT @"default_avatar.png"

typedef void (^GGDownloaderComplete)(NSData *data, GGWSError *error);

typedef void (^GGDownloaderImageComplete)(UIImage *image, GGWSError *error);
typedef void (^GGDownloaderCellImageComplete)(UIImage *image, GGWSError *error, NSIndexPath *indexPath, id identifier);

@interface GGDownloader : NSObject
{
    GGDownloaderComplete            _complete;
    GGDownloaderImageComplete       _imageComplete;
    GGDownloaderCellImageComplete   _cellImageComplete;
    
    BOOL _isRequestWithTagert;
    BOOL _isDownloadImage;
}

@property (nonatomic, strong) NSMutableURLRequest   *urlRequest;
@property (nonatomic, strong) NSString              *strURL;
@property (nonatomic, strong) NSURL                 *url;

@property (nonatomic, weak)   id                    target;
@property (nonatomic, strong) id                    identifier;

@property (nonatomic, strong) NSIndexPath           *indexPath;

//init function
- (id) initWithStringURL:(NSString*)stringURL;
- (id) initWithStringURL:(NSString*)stringURL target:(id)target;
- (id) initWithStringURL:(NSString*)stringURL target:(id)target identifier:(id)identifier;
- (id) initWithStringURL:(NSString*)stringURL target:(id)target identifier:(id)identifier indexPath:(NSIndexPath*)indexPath;

//method
- (void) setCompleteWithBlock:(GGDownloaderComplete)complete;
- (void) setImageCompleteWithBlock:(GGDownloaderImageComplete)imageComplete;
- (void) setCellImageCompleteWithBlock:(GGDownloaderCellImageComplete)cellImageComplete;

//class method
+ (void) downloadWithStringURL:(NSString*)stringURL completeWithBlock:(GGDownloaderComplete)complete;
+ (void) downloadWithStringURL:(NSString*)stringURL target:(id)target completeWithBlock:(GGDownloaderComplete)complete;
+ (void) downloadWithStringURL:(NSString*)stringURL target:(id)target identifier:(id)identifier completeWithBlock:(GGDownloaderComplete)complete;
+ (void) downloadWithStringURL:(NSString*)stringURL target:(id)target identifier:(id)identifier indexPath:(NSIndexPath*)indexPath completeWithBlock:(GGDownloaderComplete)complete;

//download image
+ (void) downLoadImageStringURL:(NSString*)stringURL completeWithBlock:(GGDownloaderImageComplete)complete;
+ (void) downLoadImageStringURL:(NSString*)stringURL target:(id)target completeWithBlock:(GGDownloaderImageComplete)complete;
+ (void) downLoadImageStringURL:(NSString*)stringURL target:(id)target identifier:(id)identifier completeWithBlock:(GGDownloaderImageComplete)complete;
+ (void) downLoadImageStringURL:(NSString*)stringURL target:(id)target identifier:(id)identifier indexPath:(NSIndexPath*)indexPath completeWithBlock:(GGDownloaderImageComplete)complete;

//download cell
+ (void) cellDownloadImageWithStringURL:(NSString*)stringURL target:(id)target identifier:(id)identifier indexPath:(NSIndexPath*)indexPath completeWithBlock:(GGDownloaderCellImageComplete)complete;

@end
