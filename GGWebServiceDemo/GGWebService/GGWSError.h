//
//  GGWSError.h
//  YouLook
//
//  Created by Tien Le Phuong on 6/4/14.
//  Copyright (c) 2014 TienLP. All rights reserved.
//

#import <Foundation/Foundation.h>

#define FXErrorNotification @"GGWSErrorNotification"

@interface GGWSError : NSObject

@property (nonatomic)           int         error_id;
@property (nonatomic, strong)   NSString    *error_msg;

+ (GGWSError*) error;

@end
