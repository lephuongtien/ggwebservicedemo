//
//  GGWSParse.h
//  YouLook
//
//  Created by Tien Le Phuong on 6/4/14.
//  Copyright (c) 2014 TienLP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GGWSError.h"
#import "NSDictionary+GGWSParseJSON.h"
#import "JSON.h"
#import "XMLDictionary.h"

typedef void (^GGWSParseBasic)(NSDictionary *dic, GGWSError *error);

@interface GGWSParse : NSObject

// Basic Parse Data
+ (NSMutableDictionary*) parseDataJSONWithData:(NSData*)data;
+ (void) parseDataJSONData:(NSData*)data WithComplete:(GGWSParseBasic)complete;

+ (NSDictionary*) parseDataXMLWithData:(NSData*)data;
+ (void) parseDataXMLData:(NSData*)data WithComplete:(GGWSParseBasic)complete;

// Custom Parse for app


@end
