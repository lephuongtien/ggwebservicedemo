//
//  GGWSRequest.m
//  YouLook
//
//  Created by Tien Le Phuong on 6/3/14.
//  Copyright (c) 2014 TienLP. All rights reserved.
//

#import "GGWSRequest.h"
#import "AFURLRequestSerialization.h"

@implementation GGWSRequest


#pragma mark - Defaulf Request
+ (NSURLRequest*) requestSimpleGETstringURL:(NSString*)strURL
{
    NSString *encodedUrl = [strURL stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    
    NSMutableURLRequest *request= [NSMutableURLRequest requestWithURL:[NSURL URLWithString:encodedUrl]
                                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                      timeoutInterval:GGWSRequestTimeOutDefault];
    
    [request setHTTPMethod:GGWSRequestMethodGET];
    
    
    return request;
}

+ (NSMutableURLRequest *)requestWithMethod:(NSString *)method
                                 URLString:(NSString *)URLString
                                parameters:(id)parameters
{
    return [[AFHTTPRequestSerializer serializer] requestWithMethod:method URLString:URLString parameters:parameters error:nil];
}


#pragma mark - Custom Request
+ (NSURLRequest*) requestGETstringURL:(NSString*)strURL
                        isAccessToken:(BOOL)isAccessToken
                      isAuthorization:(BOOL)isAuthorization
{
    NSString *encodedUrl = [strURL stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    
    NSMutableURLRequest *request= [NSMutableURLRequest requestWithURL:[NSURL URLWithString:encodedUrl]
                                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                      timeoutInterval:GGWSRequestTimeOutDefault];
    
    [request setHTTPMethod:GGWSRequestMethodGET];
    
    
    //access token
    if (isAccessToken) {
        [request setValue:@"change you access-token" forHTTPHeaderField:ACCESS_TOKEN];
    }
    
    //Authorization
    if (isAuthorization) {
        NSString* usernamepwd   = [NSString stringWithFormat:@"%@:%@",AUTHEN_USERNAME, AUTHEN_PASSWORD];
        NSData* base64          = [usernamepwd dataUsingEncoding:NSASCIIStringEncoding];
        NSString* authHeader    = [NSString stringWithFormat:@"Basic %@",  [base64 base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed]];
        
        [request setValue:authHeader          forHTTPHeaderField:@"Authorization"];
    }
    
    
    return request;
}


@end
